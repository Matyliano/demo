package com.example.demo;

import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

public class SignUpUI extends UI {

    private VerticalLayout registerLayout;
    private TextField username;
    private TextField email;
    private TextField confirmEmail;
    private PasswordField password;
    private PasswordField confirmPassword;
    private Button signUp;
    private Button back;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        addLayout();
        addHeader();
        addForm();
        addButtons();
    }

    private void addLayout() {
        registerLayout = new VerticalLayout();
        registerLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(registerLayout);
    }

    private void addHeader() {
        Label header = new Label("Sign Up");
        header.addStyleName(ValoTheme.LABEL_H1);
        registerLayout.addComponent(header);
    }

    private void addForm() {
        username = new TextField("Username");
        email = new TextField("Email");
        confirmEmail = new TextField("Confirm Email");
        password = new PasswordField("Passsword");
        confirmPassword = new PasswordField("Confirm Password");


    }

    private void addButtons() {
        HorizontalLayout horizontal = new HorizontalLayout();
        signUp = new Button("sign up");
        back = new Button("back");
        horizontal.addComponents(signUp,back);
        registerLayout.addComponent(horizontal);
    }
}
