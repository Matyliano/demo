package com.example.demo;

import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

@SpringUI
public class ConferenceUI extends UI {


    private VerticalLayout root;


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        addLayout();
        addHeader();
        addGrid();
        addButtons();
    }

    private void addLayout() {
        root = new VerticalLayout();
        root.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(root);
    }

    private void addHeader() {
        Label header = new Label("IT CONFERENCE");
        header.addStyleName(ValoTheme.LABEL_H1);
        root.addComponent(header);

    }

    private void addGrid() {

    }

    private void addButtons() {
        HorizontalLayout horizontal = new HorizontalLayout();
        Button login = new Button("login");
        Button signUp = new Button("sign up");
        horizontal.addComponents(login,signUp);
        root.addComponent(horizontal);
    }
}
