package com.example.demo;

import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

public class LoginUI extends UI {

    private VerticalLayout loginLayout;
    private TextField username;
    private PasswordField password;
    private Button login;
    private Button back;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        addLayout();
        addHeader();
        addForm();
        addButtons();
    }

    private void addLayout() {
        loginLayout = new VerticalLayout();
        loginLayout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(loginLayout);
    }

    private void addHeader() {
        Label header = new Label("Login");
        header.addStyleName(ValoTheme.LABEL_H1);
        loginLayout.addComponent(header);
    }

    private void addForm() {
        username = new TextField("Username");
        password = new PasswordField("password");
    }

    private void addButtons() {
        HorizontalLayout horizontal = new HorizontalLayout();
         login = new Button("login");
         back = new Button("back");
        horizontal.addComponents(login,back);
        loginLayout.addComponent(horizontal);
    }
}
